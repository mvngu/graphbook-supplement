The neural network of the nematode C. Elegans.  The dataset was
compiled by (Watts & Strogatz 1998) from original experimental data by
(White et al. 1986).  Here is an explanation of the files:

* node-weight.dat -- the weight of each node.  Each line follows the
  format "u w", where u is a vertex ID and w is the weight of the
  vertex.  Each vertex corresponds to a neuron.

* celegans.edge -- the weighted edges.  Each line follows the format
  "u v w", where "u v" denotes a directed edge from u to v and w is
  the weight of the edge.

These data can be cited as:

@article{WattsStrogatz1998,
  author  = {Duncan J. Watts and Steven H. Strogatz},
  title   = {Collective dynamics of `small-world' networks},
  journal = {Nature},
  year    = {1998},
  volume  = {393},
  pages   = {440--442},
  doi     = {10.1038/30918},
}

@article{WhiteEtAl1986,
  author  = {J. G. White and E. Southgate and J. N. Thompson and S. Brenner},
  title   = {The Structure of the Nervous System of the Nematode
             {C}aenorhabditis elegans},
  journal = {Phil. Trans. R. Soc. Lond. B},
  year    = {1986},
  volume  = {314},
  pages   = {1--340},
  doi     = {10.1098/rstb.1986.0056},
}

Nodes:   297
Edges: 2,359
